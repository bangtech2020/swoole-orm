<?php
/**
 * User: sethink
 */

namespace bangtech\swooleOrm;

use RuntimeException;
use Swoole;

class MysqlPool
{
    /**
     * 连接池
     * @var Swoole\Coroutine\Channel
     */
    protected $pool;

    //池状态
    protected $available = true;

    //新建时间
    protected $addPoolTime = '';

    //入池时间
    protected $pushTime = 0;

    //配置
    public $config = [
        //服务器地址
        'host'      => '127.0.0.1',
        //端口
        'port'      => 3306,
        //用户名
        'user'      => '',
        //密码
        'password'  => '',
        //数据库编码，默认为utf8
        'charset'   => 'utf8',
        //数据库名
        'database'  => '',
        //表前缀
        'prefix'    => '',
        //空闲时，保存的最大链接，默认为5
        'poolMin'   => 5,
        //地址池最大连接数，默认1000
        'poolMax'   => 1000,
        //清除空闲链接的定时器，默认8h-8s(8s的配置保证不卡时间点)
        'clearTime' => 59*60*8,
        //空闲多久清空所有连接,默认300s
        'clearAll'  => 300,
        //设置是否返回结果
        'setDefer'  => true
    ];


    public function __construct($config)
    {
        if (isset($config['clearAll'])) {
            if ($config['clearAll'] < $config['clearTime']) {
                $config['clearAll'] = (int)($config['clearTime'] / 1000);
            } else {
                $config['clearAll'] = (int)($config['clearAll'] / 1000);
            }
        }

        $this->config = array_merge($this->config, $config);
        $this->pool   = new Swoole\Coroutine\Channel($this->config['poolMax']);
    }


    /**
     * @入池
     *
     * @param $mysql
     */
    public function put($mysql)
    {
        if ($this->pool->length() < $this->config['poolMax']) {
            $this->pool->push([$mysql,time()]);
        }
    }

    /**
     * @return Swoole\Coroutine\Mysql
     */
    protected function createMysql()
    {
        $mysql = new Swoole\Coroutine\Mysql();

        $mysql->connect([
            'host'     => $this->config['host'],
            'port'     => $this->config['port'],
            'user'     => $this->config['user'],
            'password' => $this->config['password'],
            'charset'  => $this->config['charset'],
            'database' => $this->config['database']
        ]);

        return $mysql;
    }


    /**
     * @出池
     *
     * @return bool|mixed|Swoole\Coroutine\Mysql
     */
    public function get()
    {
        $re_i = -1;

        back:
        $re_i++;

        if (!$this->available) {
            throw new RuntimeException(date('Y-m-d H:i:s', time()) . "：Mysql连接池正在销毁");
        }

        //有空闲连接且连接池处于可用状态
        if ($this->pool->length() > 0) {
            $mysql = $this->pool->pop();
            //检查连接池里面的连接是否超过MySQL超时配置[Connection reset by peer]
            if (($mysql[1] + $this->config['clearTime']) < time()){
                try {
                    $mysql[0]->close();
                }catch (\Throwable $exception){
                    //不处理数据库关闭错误
                }
                $mysql = $this->createMysql();
            }else{
                $mysql = $mysql[0];
            }
        } else {
            //无空闲连接，创建新连接
            $mysql = $this->createMysql();
        }

        if ($mysql->connected === true && $mysql->connect_error === '') {
            return $mysql;
        } else {
            if ($re_i <= $this->config['poolMin']) {
                $this->dumpError("mysql-重连次数{$re_i}，[errCode：{$mysql->connect_errno}，errMsg：{$mysql->connect_error}]");

                $mysql->close();
                unset($mysql);
                goto back;
            }
            throw new RuntimeException(date('Y-m-d H:i:s', time()) . "：Mysql重连失败");
        }
    }


    /**
     * @定时器
     *
     * @param $server
     */
    public function clearTimer($server)
    {
        $server->tick($this->config['clearTime'], function () use ($server) {

            if ($this->pool->length() > $this->config['poolMin'] && time() - 5 > $this->addPoolTime) {
                $this->pool->pop();
            }


            if ($this->pool->length() > 0 && time() - $this->config['clearAll'] > $this->pushTime) {
                while (!$this->pool->isEmpty()) {
                    $this->pool->pop();
                }
            }
        });
    }


    /**
     * @打印错误信息
     *
     * @param $msg
     */
    public function dumpError($msg)
    {
        var_dump(date('Y-m-d H:i:s', time()) . "：{$msg}");
    }


    public function destruct()
    {
        // 连接池销毁, 置不可用状态, 防止新的客户端进入常驻连接池, 导致服务器无法平滑退出
        $this->available = false;
        while (!$this->pool->isEmpty()) {
            $this->pool->pop();
        }
    }


    public function getPoolSum()
    {
        return $this->pool->length();
    }

}