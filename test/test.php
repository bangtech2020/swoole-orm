<?php

require __DIR__ . "/../vendor/autoload.php";

/*
require __DIR__ . "/../src/Db.php";
require __DIR__ . "/../src/MysqlPool.php";
require __DIR__ . "/../src/db/Builder.php";
require __DIR__ . "/../src/db/Query.php";
*/

use bangtech\swooleOrm\MysqlPool;

$config = [
    'host' => '118.89.79.241', //服务器地址
    'port' => 3306,        //端口
    'user' => 'bus',  //用户名
    'password' => 'biHwGiW4aTiZ4xJj',      //密码
    'charset' => 'utf8',   //编码
    'database' => 'bus',   //数据库名
    'prefix' => '',        //表前缀
    'poolMin' => 5,        //空闲时，保存的最大链接，默认为5
    'poolMax' => 1000,     //地址池最大连接数，默认1000
    'clearTime' => 60000,   //清除空闲链接定时器，默认60秒，单位ms
    'clearAll' => 300000,  //空闲多久清空所有连接，默认5分钟，单位ms
    'setDefer' => true,    //设置是否返回结果,默认为true,
];
$count = \bangtech\swooleOrm\Db::init(new MysqlPool($config))->table('authorization')->count();

var_dump($count);